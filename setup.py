from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
# from setuptools.command.
import sys
import os
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from setuptools import Extension
import numpy as np
import scipy
import platform
from pathlib import Path

root_dir = Path.cwd()
dnpf_path = root_dir / 'dnpf'

# Manual definition of custom cython extensions
extensions = [Extension("dnpf.cython_extension.powerflow",
                        [(dnpf_path / "cython_extension" / "powerflow.pyx").as_posix(),
                         (dnpf_path / "cython_extension" / "network.cpp").as_posix()])]

compiler_directives = {
    'language_level': 3,
    'boundscheck': True,
    'initializedcheck': True,
    'nonecheck': True,
    'cdivision': False,
}


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest

        errcode = pytest.main(self.test_args)
        sys.exit(errcode)


# Automatic collection of data files
data_extensions = ['.csv', '.hdf', '.json', '.geojson', '.xml', '.gml', '.xls', '.epw', '.RAY', '.txt', '.TMY']
data_files = []
for r, d, f in os.walk(dnpf_path):
    for file in f:
        for ext in data_extensions:
            if ext in file:
                data_files.append(file)

__version__ = '0.0.1'

setup(
    name='dnpf',
    version=__version__,
    author='',
    author_email='',
    packages=find_packages(),
    url='',
    license='See LICENSE.txt',
    description="Distribution Network Power Flow",
    package_data={'dnpf': data_files, },
    long_description=open('README.md').read(),
    extras_require={
    },
    entry_points={
        'console_scripts': [
        ]},
    classifiers=[
        "Development Status ::  Alpha",
    ],
    tests_require=['pytest', 'pytest-cov'],
    cmdclass={'test': PyTest,
              'build_ext': build_ext
              },
    ext_modules=cythonize(extensions, annotate=True, compiler_directives=compiler_directives),
    zip_safe=False
)
