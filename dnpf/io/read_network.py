import pandas as pd
import re
import geopandas as gpd
from shapely import wkt
from dnpf.io.get_network_params import create_network_graph
import networkx as nx

def set_bus_position(buses):

    geom_ = gpd.GeoSeries(buses.geometry.apply(wkt.loads))
    buses['x'] = geom_.x
    buses['y'] = geom_.y


def reverse_line_direction(buses, lines, sub_id):
    """

    :param buses:
    :param lines:
    :param sub_id:
    :return:
    """

    if lines.iloc[0]['from'] != sub_id:
        raise ValueError('Substation must the source node of the first line')

def set_bus_int_id(buses, lines, sub_id):
    """

    :param buses:
    :param lines:
    :return:
    """
    sub_id_line = lines.loc[lines.source == sub_id].index[0]
    lines = pd.concat([lines.loc[[sub_id_line], :], lines.drop(sub_id_line, axis=0)], axis=0)
    bus_names = [sub_id] + lines.target.unique().tolist()

    bus_replace_dict = {bus_name: i for i, bus_name in enumerate(bus_names)}
    buses['id'] = buses.index
    buses[['id']] = buses[['id']].replace(bus_replace_dict)
    lines['from'] = lines['source']
    lines['to'] = lines['target']
    lines[['from', 'to']] = lines[['from', 'to']].replace(bus_replace_dict)
    buses = buses.sort_values('id')

    return buses, lines


def graph_standardize(buses, lines, sub_id, rename_line_columns=True, rename_line_dict=None):
    """

    :param buses:
    :param lines:
    :return:
    """
    if rename_line_columns:
        if rename_line_dict is None:
            rename_line_dict = {
                'from': 'source',
                'to': 'target',
            }
        for key, val in rename_line_dict.items():
            lines[key] = lines[val]

    dfs_network, network_graph = create_network_graph(sub_id, buses, lines)

    lines_from_graph = nx.to_pandas_edgelist(network_graph)
    lines_from_graph.rename(columns={'source': 'from', 'target': 'to'}, inplace=True)
    buses_from_graph = pd.DataFrame.from_dict(dict(network_graph.nodes(data=True)), orient='index')
    buses_from_graph.drop(columns=['position'], inplace=True)
    buses = buses_from_graph.merge(buses, how='left', left_index=True, right_on='bus_id')

    lines.drop(columns=['from', 'to'], inplace=True)
    lines = lines_from_graph.merge(lines, how='left', left_on='branch_id', right_on='branch_id')
    sub_id_line = lines.loc[lines['from'] == sub_id].index[0]
    lines = pd.concat([lines.loc[[sub_id_line], :], lines.drop(sub_id_line, axis=0)], axis=0)
    bus_names = buses['bus_id'].values.tolist()

    bus_replace_dict = {bus_name: i for i, bus_name in enumerate(bus_names)}
    buses['id'] = buses['bus_id']
    buses[['id']] = buses[['id']].replace(bus_replace_dict)

    lines[['from', 'to']] = lines[['from', 'to']].replace(bus_replace_dict)
    buses = buses.sort_values('id')
    lines = lines.sort_values('to')

    return buses, lines


def read_network_hdf(path):
    """

    :param path:
    :return:
    """

    store = pd.HDFStore(path)
    store_keys = store.keys()
    p = re.compile("/feeders/(.*)/lines")
    feeder_list = [p.search(s_k).group(1) for s_k in store_keys[2:] if 'lines' in s_k]

    with store as hdf_store:

        substation = hdf_store.get('/substation')
        feeders = {}

        for feeder in feeder_list:
            buses = hdf_store.get(f'/feeders/{feeder}/buses')
            lines = hdf_store.get(f'/feeders/{feeder}/lines')

            buses, lines = set_bus_int_id(buses, lines, substation.index[0])

            feeders[feeder] = {
                'buses': buses,
                'lines': lines
            }

    return {
        'substation': substation,
        'feeders': feeders
    }


def read_network_csv(path, rename_line_columns=False, rename_line_dict=None, linear_parameters_dict=None,
                     length='length', set_bus_xy=False):
    """

    :param path:
    :return:
    """


    substation = pd.read_csv(path / 'Transfo 0.csv', index_col=0)
    per_unit = pd.read_csv(path / 'per unit scale.csv')
    substation.loc[substation.index[0], 'nominal_voltage'] = per_unit['voltage'].values
    substation.loc[substation.index[0], 'setpoint'] = substation.loc[substation.index[0], substation.columns[0]] / substation.loc[substation.index[0], 'nominal_voltage']

    feeder_dirs = [e for e in path.iterdir() if e.is_dir()]
    feeder_list = [dir.parts[-1] for dir in feeder_dirs]
    feeders = {}

    for feeder in feeder_list:
        buses = pd.read_csv(path / feeder / 'nodes.csv')
        lines = pd.read_csv(path / feeder / 'lines.csv')
        if linear_parameters_dict is not None:
            for key, val in linear_parameters_dict.items():
                lines[key] = lines[val] * lines[length]

        if set_bus_xy:
            set_bus_position(buses)

        buses, lines = graph_standardize(buses, lines, substation.index[0],
                                         rename_line_columns=rename_line_columns,
                                         rename_line_dict=rename_line_dict)
        feeders[feeder] = {
            'buses': buses,
            'lines': lines
        }

    return {
        'substation': substation,
        'feeders': feeders
    }