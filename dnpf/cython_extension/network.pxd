# distutils: language = c++
from libcpp.vector cimport vector

cdef extern from "network.cpp":
    pass

cdef extern from "network.h" namespace "power":
    cdef cppclass Network:
        Network(vector[double], vector[double], vector[int], vector[int]) except +
        vector[int] fro
        vector[int] to
        vector[double] r
        vector[double] x
        vector[map[int,vector[int]]] get_nodes()
        void Backward(vector[double] Pload,vector[double] Qload,
        vector[double] P,vector[double] Q,vector[double] U)
        void Forward(vector[double] Pload,vector[double] Qload,
        vector[double] P,vector[double] Q,vector[double] U)
        vector[vector[double]] BackwardForward_Sweep(vector[double] U,
        vector[double] Pload,vector[double] Qload,double prec, int max_iter)