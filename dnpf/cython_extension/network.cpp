#include "network.h"
#include <iostream>

using namespace power;

node_map create_node_list(std::vector<int> fro, std::vector<int> to){

    node_map node_list;

    // we obtain a map of the nodes with their children
    std::map<int, std::vector<int> > child_node;
    std::vector<int> empty_vec;

    for (int j = 0; j <= fro.size(); j++){
        child_node[j] = empty_vec;
    }

    for (int j = 0; j < fro.size(); j++){
        child_node[fro[j]].push_back(to[j]);
    }


    // then we group the nodes by levels
    std::vector<int> level_node_list;
    int level_counter = 0;
    level_node_list.push_back(0);
    std::vector<int> next_level_node_list;
    std::map<int, std::vector<int> >::iterator child_node_it = child_node.begin();

    while (child_node_it != child_node.end()){

        std::vector<int>::iterator level_node_it = level_node_list.begin();
        std::map<int, std::vector<int> > node_list_tmp;

        for (level_node_it; level_node_it != level_node_list.end(); ++level_node_it){

            if(child_node_it != child_node.end()){


            next_level_node_list.insert(next_level_node_list.end(),child_node[*level_node_it].begin(),child_node[*level_node_it].end());
            node_list_tmp.insert(std::pair<int, std::vector<int> > (*level_node_it, child_node[*level_node_it]));
            ++child_node_it;
            }
        }
        level_node_list = next_level_node_list;
        next_level_node_list.erase(next_level_node_list.begin(), next_level_node_list.end());
        ++level_counter;
        node_list.push_back(node_list_tmp);
}


return node_list;
};

Network::Network() {}

Network::Network( std::vector<double> R, std::vector<double> X, std::vector<int> Fro, std::vector<int> To)
{
	this->r = R;
	this->x = X;
	this->fro = Fro;
	this->to = To;
	this->nodes = create_node_list(Fro, To);
}

Network::~Network()
{
}

node_map Network::get_nodes()
{
	return nodes;
}


double solve_degree2(double a,double b,double c, double down, int mode)
{
  double delta,x1,x2;
  delta= b*b-4*a*c ;
	if (delta<0){
		return(0);
	}
	else{
		x1=(-b-(sqrt(delta)))/(2*a);
		x2=(-b+(sqrt(delta)))/(2*a);
	}
	switch (mode)
	{
		case 1: {
			return(x1);
		}
		case 2: {
			return(x2);
		}
		case 3: {// min(X1,X2)
			if (down<0){ return(x2);}

      if (down>0){ if (x1>down){return(x2);}else{return(x1);}}

			return(x2);
		}
		case 4: {//max(X1,X2)
			if (x2>x1){ return(x2); }else{ return(x1);}
			return(x2);
		}
	}
};

void Network::Backward(std::vector<double> Pload,std::vector<double> Qload,
	std::vector<double> &P,std::vector<double> &Q,std::vector<double> &U ){
		int father_ind;
		std::vector<int> son_ind;
		double a,b,c,down;

		std::vector< std::map<int,std::vector<int> > >::reverse_iterator _nodes_rev_it=nodes.rbegin();

		while (_nodes_rev_it!=nodes.rend())
		{
			std::map<int, std::vector<int> >::iterator  nodestmp_it = (*_nodes_rev_it).begin();

			while (nodestmp_it!=(*_nodes_rev_it).end())
			{// traite la hauteur
				father_ind=nodestmp_it->first;

				if (father_ind!=0)
				{// pas à la racine
				if ((nodestmp_it->second).empty())
					{// the current node is a leaf
						a=-r[father_ind-1]/U[fro[father_ind-1]];
						b=1;
						c=Q[father_ind]*Q[father_ind]*a-Pload[father_ind];
            			down = Pload[father_ind];

						//for (int i=0;i<son_ind.size();i++){c-=P[son_ind[i]];}
						P[father_ind]=solve_degree2(a,b,c,down,3);

						/// determination de Q
						a=-x[father_ind-1]/U[fro[father_ind-1]];
						c=P[father_ind]*P[father_ind]*a-Qload[father_ind];
             			down = Qload[father_ind];
						//for (int i=0;i<son_ind.size();i++){c-=Q[son_ind[i]];}
						Q[father_ind]=solve_degree2(a,b,c,down,3);
					}else
					{
					/// determination de P
						a=-r[father_ind-1]/U[fro[father_ind-1]];
						b=1;
						c=Q[father_ind]*Q[father_ind]*a-Pload[father_ind];
            			down = Pload[father_ind];
						son_ind=nodestmp_it->second;
						for (int i=0;i<son_ind.size();i++){
							c-=P[son_ind[i]];
              				down+=P[son_ind[i]];
						}
						P[father_ind]=solve_degree2(a,b,c,down,3);

						/// determination de Q
						a=-x[father_ind-1]/U[fro[father_ind-1]];
						c=P[father_ind]*P[father_ind]*a-Qload[father_ind];
            			down = Qload[father_ind];
  					for (int i=0;i<son_ind.size();i++){
							c-=Q[son_ind[i]];
              				down+=Q[son_ind[i]];
						}
						Q[father_ind]=solve_degree2(a,b,c,down,3);
					}

				}
				/// determination de P
				// incrementation du noeud p?re, toujours ? la m?me hauteur
				++nodestmp_it;
			}
			++_nodes_rev_it; //incrementation sur la hauteur

		}


	};

void Network::Forward(std::vector<double> Pload,std::vector<double> Qload,
	std::vector<double> &P,std::vector<double> &Q,std::vector<double> &U){

		int father_ind;
		std::vector<int> son_ind;
		double a;
		std::vector<std::map<int,std::vector<int> > >::iterator _nodes_it=nodes.begin();
		while (_nodes_it!=nodes.end())
		{// on part du bas, et on iter sur la hauteur de l'arbre.

			std::map<int,std::vector<int> >::iterator  nodestmp_it= (*_nodes_it).begin();
			while (nodestmp_it!=(*_nodes_it).end())
			{// traite le noeud
				father_ind=nodestmp_it->first;
				son_ind=nodestmp_it->second;


				if (!son_ind.empty())
				{
					/// determination de U
					for (int i=0;i<son_ind.size();i++){
                        a=(Q[son_ind[i]]*Q[son_ind[i]]+P[son_ind[i]]*P[son_ind[i]])/U[father_ind];
						U[son_ind[i]]=a*(r[son_ind[i]-1]*r[son_ind[i]-1]+x[son_ind[i]-1]*x[son_ind[i]-1])+U[father_ind];
						U[son_ind[i]]-=2*(x[son_ind[i]-1]*Q[son_ind[i]]+r[son_ind[i]-1]*P[son_ind[i]]);
					}
					// incrementation du noeud p?re
				}
				++nodestmp_it;
			}
			++_nodes_it;
		}

	};

std::vector<std::vector<double> > Network::BackwardForward_Sweep(std::vector<double> U,
	std::vector<double> Pload,std::vector<double> Qload,double prec, int max_iter){

		int n=Qload.size(),cpt=1;
		std::vector<double> Q(n,0.),P(n,0.),Uprec(n,0.), DiffNeg(n,0.), DiffPos(n,0.);
		double maxdiffpos, maxdiffneg;

		this->Backward(Pload,Qload,P,Q,U);

		this->Forward(Pload,Qload,P,Q,U);

		do {
			cpt++;
			Uprec=U;
			this->Backward(Pload,Qload,P,Q,U); this->Forward(Pload,Qload,P,Q,U);
			for (int t = 0 ; t < U.size(); t++){
			DiffNeg[t] = U[t]-Uprec[t];
			DiffPos[t] = Uprec[t]-U[t];
			}

			std::vector<double>::iterator maxneg = std::max_element(DiffNeg.begin(), DiffNeg.end());
			std::vector<double>::iterator maxpos = std::max_element(DiffPos.begin(), DiffPos.end());

			maxdiffneg = *maxneg;
			maxdiffpos = *maxpos;

			if (cpt > max_iter)
			{
//			std::cout << "max iteration exceeded" << std::endl;
			break;
			}

		} while((maxdiffneg > prec) || (maxdiffpos > prec)) ;

		std::vector<std::vector<double> > result;
		std::vector<double> cnt(cpt);
		result.push_back(P);
		result.push_back(Q);
		result.push_back(U);
		result.push_back(cnt);
		return result;
	};


