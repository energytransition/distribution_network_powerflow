#include <vector>
#include <map>
#include <list>
#include <algorithm>
#include <iterator>
#include <functional>
#include <iostream>
#include <math.h>


typedef std::vector< std::map< int, std::vector<int> > > node_map;

namespace power {
    class Network {
        public :
            std::vector<int> fro;
            std::vector<int> to;
            std::vector<double> r;
            std::vector<double> x;
            node_map nodes;
            Network();
            Network(std::vector<double> r , std::vector<double> x, std::vector<int> fro, std::vector<int> to);
            ~Network();
            node_map get_nodes();
            void Backward(std::vector<double> Pload,std::vector<double> Qload,
            std::vector<double> &P,std::vector<double> &Q,std::vector<double> &U);
            void Forward(std::vector<double> Pload,std::vector<double> Qload,
            std::vector<double> &P,std::vector<double> &Q,std::vector<double> &U);
            std::vector< std::vector<double> > BackwardForward_Sweep(std::vector<double> U,
            std::vector<double> Pload,std::vector<double> Qload, double prec, int max_iter);
        };
}