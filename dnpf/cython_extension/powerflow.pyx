# distutils: language = c++
from libcpp.vector cimport vector
from libcpp.map cimport map
import numpy as np
cimport numpy as np
DTYPE = np.double
ctypedef np.double_t DTYPE_t


cdef extern from "network.h" namespace "power":
    cdef cppclass Network:
        Network() except +
        Network(vector[double], vector[double], vector[int], vector[int]) except +
        vector[double] r
        vector[double] x
        vector[int] fro
        vector[int] to
        vector[map[int,vector[int]]] get_nodes()
        void Backward(vector[double] Pload,vector[double] Qload,
        vector[double] P,vector[double] Q,vector[double] U)
        void Forward(vector[double] Pload,vector[double] Qload,
        vector[double] P,vector[double] Q,vector[double] U)
        vector[vector[double]] BackwardForward_Sweep(vector[double] U,
        vector[double] Pload,vector[double] Qload,double prec, int max_iter)


cdef class PyNetwork:
    cdef Network c_network
    def __init__(self, vector[double] r, vector[double] x, vector[int] fro, vector[int] to):
        self.c_network = Network(r, x, fro, to)

    def get_nodes(self):
        return self.c_network.get_nodes()

    def BackwardForward_Sweep(self, vector[double] U,
	vector[double] Pload,vector[double] Qload,double prec, int max_iter):
        return self.c_network.BackwardForward_Sweep(U, Pload, Qload, prec, max_iter)

    def multiBackwardForward_Sweep(self, np.ndarray[DTYPE_t, ndim=2] U,np.ndarray[DTYPE_t, ndim=2] Pload,
                              np.ndarray[DTYPE_t, ndim=2] Qload,double prec, int max_iter):

        cdef int node_cnt=Qload.shape[0]
        cdef int step_cnt=Qload.shape[1]
        Q = np.zeros([node_cnt, step_cnt], dtype=DTYPE)
        P = np.zeros([node_cnt, step_cnt], dtype=DTYPE)
        Ures = np.zeros([node_cnt, step_cnt], dtype=DTYPE)

        for t in range(step_cnt):
            result = self.BackwardForward_Sweep(U[:,t], Pload[:,t], Qload[:,t], prec, max_iter)
            Ures[:,t] = result[2]
            P[:,t] = result[0]
            Q[:,t] = result[1]

        result = [P, Q, Ures]
        return result







    