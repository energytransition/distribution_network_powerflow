import plotly.express as px
import pandas as pd
import plotly.graph_objs as go
import numpy as np

max_width = 10.
min_width = 5.
node_size = 5.


def plot_feeder_powerflow(buses, lines, results, step=0, nominal_voltage=2.e4, voltage_limits=(0.95, 1.05), fig=None):
    """

    :param buses:
    :param lines:
    :return:
    """
    buses_to_plot = pd.concat([buses, results['voltage'].loc[:, step]], axis=1)
    buses_to_plot.rename(columns={step: 'voltage'}, inplace=True)
    buses_to_plot = pd.concat([buses_to_plot, results['active_load'].loc[:, step]], axis=1)
    buses_to_plot.rename(columns={step: 'active_load'}, inplace=True)
    lines_to_plot = pd.concat([lines, results['active_flow'].loc[:, step]], axis=1)
    lines_to_plot.rename(columns={step: 'active_flow'}, inplace=True)

    if fig is None:
        fig = go.Figure()

    max_flow = lines_to_plot.active_flow.max()
    min_flow = lines_to_plot.active_flow.min()
    max_load = buses_to_plot.active_load.max()

    for i, line in lines_to_plot.iterrows():
        line_buses = buses_to_plot.loc[buses_to_plot.id.isin([line['from'], line['to']])]

        if max_flow != 0.:
            fig.add_trace(go.Scatter3d(x=line_buses['x'],
                                       y=line_buses['y'],
                                       z=line_buses['voltage'],
                                       opacity=0.3,
                                       mode='lines',
                                       showlegend=False,
                                       line={
                                           'width': max_width * (line['active_flow'] - min_flow) / max_flow + min_width,
                                           'color': 'black'}))

        # for vol_lim in voltage_limits[0]:
        fig.add_trace(go.Scatter3d(x=line_buses['x'],
                                   y=line_buses['y'],
                                   z=nominal_voltage * voltage_limits[0] * np.ones((2, )),
                                   mode='lines',
                                   showlegend=False,
                                   line={
                                       'width': max_width / 4.,
                                        'color': 'black'}))
    if max_load != 0.:
        fig.add_trace(go.Scatter3d(x=buses_to_plot['x'],
                                   y=buses_to_plot['y'],
                                   z=buses_to_plot['voltage'],
                                   mode='markers',
                                   showlegend=False,
                                   marker={'size': max_width / 1.5 * (buses_to_plot['active_load'].values) / max_load,
                                           'color': buses_to_plot['voltage'],
                                           'colorscale': 'Electric',
                                           'opacity': 1.0,
                                           'line': {'width': 0},
                                           'cmin': nominal_voltage * voltage_limits[0],
                                           'cmax': nominal_voltage * voltage_limits[1]}))

    # res = 10
    # x = np.linspace(buses_to_plot['x'].min(), buses_to_plot['x'].max(), res)
    # y = np.linspace(buses_to_plot['y'].min(), buses_to_plot['y'].max(), res)
    # X, Y = np.meshgrid(x, y)
    # for vol_lim in voltage_limits:
    #     fig.add_trace(go.Surface(x=X, y=Y, z=np.ones((res, res)) * nominal_voltage * vol_lim,
    #                              showscale=False, opacity=0.5))

    return fig


def plot_network_powerflow(network_data, results, step=0, nominal_voltage=2.e4, voltage_limits=(0.95, 1.05)):
    """

    :param network_data:
    :param results:
    :param step:
    :param nominal_voltage:
    :param voltage_limits:
    :return:
    """
    fig = go.Figure()

    for feeder_id, feeder_data in network_data['feeders'].items():
        fig = plot_feeder_powerflow(feeder_data['buses'],
                                    feeder_data['lines'],
                                    results[feeder_id],
                                    step,
                                    nominal_voltage,
                                    voltage_limits,
                                    fig)

    fig.update_layout(template="none",
                      margin=dict(l=0, r=0, b=0, t=0),
                      scene=dict(aspectmode='manual', aspectratio=dict(x=1, y=1, z=0.8)))
    return fig
