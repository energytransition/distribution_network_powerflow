
from pkg_resources import resource_filename

data_path = {
    'data': resource_filename('dnpf', 'data'),
    'mv_network': resource_filename('dnpf', 'data/mv_network'),
    'mv_network_csv': resource_filename('dnpf', 'data/mv_network_csv')
}