import pandas as pd
import numpy as np
from dnpf import data_path
from pathlib import Path
from dnpf.io.read_network import read_network_hdf
from dnpf.simulation.powerflow import network_powerflow
from dnpf.plot.powerflow import plot_network_powerflow


def test_powerflow_hdf():
    sub_id = "HVMV02"

    mv_network_path = Path(data_path['mv_network']) / f"network_{sub_id}.hdf"

    network_data = read_network_hdf(mv_network_path)

    step_count = 2
    max_load = 1.5e5
    active_load = {}
    reactive_load = {}
    for feeder_id, feeder_data in network_data['feeders'].items():
        active_load_ = pd.DataFrame(index=feeder_data['buses'].index, columns=range(step_count), data=0.)
        load_mask = active_load_.index.str.contains('MVLV')
        active_load_.loc[load_mask, :] = np.random.random((int(load_mask.sum()), step_count)) * max_load
        active_load[feeder_id] = active_load_
        reactive_load[feeder_id] = active_load_ * 0.2

    substation = network_data['substation']
    per_unit_scale = {
        'voltage': substation['nominal_voltage'].values[0],
        'power': 3.e6  # 3 MW
    }

    results = network_powerflow(network_data,
                                active_load,
                                reactive_load,
                                per_unit_scale,
                                precision=1e-8,
                                max_iteration=10)

    fig = plot_network_powerflow(network_data, results)
    fig.show()
    pass


if __name__ == '__main__':
    test_powerflow_hdf()
