import pandas as pd
import numpy as np
from dnpf import data_path
from pathlib import Path
from dnpf.io.read_network import read_network_hdf
from dnpf.simulation.powerflow import feeder_powerflow
from dnpf.plot.powerflow import plot_feeder_powerflow

def test_powerflow_hdf():
    sub_id = "HVMV02"

    mv_network_path = Path(data_path['mv_network']) / f"network_{sub_id}.hdf"

    network_data = read_network_hdf(mv_network_path)

    test_feeder = network_data['feeders']['MVFeeder034']
    substation = network_data['substation']
    per_unit_scale = {
        'voltage': substation['nominal_voltage'].values[0],
        'power': 3.e6  # 3 MW
    }

    step_count = 2
    max_load = 1.5e6
    active_load = pd.DataFrame(index=test_feeder['buses'].index, columns=range(step_count), data=0.)
    load_mask = active_load.index.str.contains('MVLV')
    active_load.loc[load_mask, :] = np.random.random((int(load_mask.sum()), step_count)) * max_load
    # active_load.loc[load_mask, :] = max_load
    reactive_load = active_load * 0.2

    results = feeder_powerflow(test_feeder['lines'],
                               test_feeder['buses'],
                               active_load,
                               reactive_load,
                               substation['nominal_voltage'].values[0],
                               substation['setpoint'].values[0],
                               per_unit_scale,
                               precision=1e-8,
                               max_iteration=10)

    fig = plot_feeder_powerflow(test_feeder['buses'], test_feeder['lines'], results)
    fig.show()
    pass




if __name__ == '__main__':
    test_powerflow_hdf()
