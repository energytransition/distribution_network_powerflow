import pandas as pd
import numpy as np
from dnpf import data_path
from pathlib import Path
from dnpf.io.read_network import read_network_hdf, read_network_csv
from dnpf.simulation.powerflow import feeder_powerflow
from dnpf.plot.powerflow import plot_network_powerflow, plot_feeder_powerflow


def test_powerflow_csv():
    sub_id = "network1566"

    mv_network_path = Path(data_path['mv_network_csv']) / sub_id
    rename_line_dict = {'R': 'resistance', 'X': 'reactance', 'Imax': 'power_limit'}
    network_data = read_network_csv(mv_network_path,
                                    rename_line_columns=True,
                                    rename_line_dict=rename_line_dict)

    test_feeder = network_data['feeders']['Feeder 0']
    substation = network_data['substation']
    per_unit_scale = {
        'voltage': substation['nominal_voltage'].values[0],
        'power': 3.e6  # 3 MW
    }

    step_count = 10
    max_load = 5.e4
    active_load = pd.DataFrame(index=test_feeder['buses'].index, columns=range(step_count), data=0.)
    load_mask = test_feeder['buses']['max_power'] > 0.
    active_load.loc[load_mask, :] = np.random.random((int(load_mask.sum()), step_count)) * max_load
    reactive_load = active_load * 0.2

    results = feeder_powerflow(test_feeder['lines'],
                               test_feeder['buses'],
                               active_load,
                               reactive_load,
                               substation['nominal_voltage'].values[0],
                               substation['setpoint'].values[0],
                               per_unit_scale,
                               precision=1e-8,
                               max_iteration=10)
    fig = plot_feeder_powerflow(test_feeder['buses'], test_feeder['lines'], results)
    fig.show()
    pass


if __name__ == '__main__':
    test_powerflow_csv()
