import pandas as pd
import numpy as np
from dnpf import data_path
from pathlib import Path
from dnpf.io.read_network import read_network_hdf, read_network_csv
from dnpf.simulation.powerflow import feeder_powerflow
from dnpf.plot.powerflow import plot_network_powerflow, plot_feeder_powerflow


def run_roseaux_test_case(feeder_name, to_plot=True):
    sub_id = "roseaux_test_data"

    mv_network_path = Path(data_path['mv_network_csv']) / sub_id

    rename_line_dict = {
                'from': 'bus1_id',
                'to': 'bus2_id',
                'Imax': 'maximal_current'
            }
    linear_parameters_dict = {
                'R': 'r',
                'X': 'x'
            }

    network_data = read_network_csv(mv_network_path,
                                    rename_line_columns=True,
                                    rename_line_dict=rename_line_dict,
                                    linear_parameters_dict=linear_parameters_dict,
                                    set_bus_xy=True)

    test_feeder = network_data['feeders'][feeder_name]
    substation = network_data['substation']
    per_unit_scale = {
        'voltage': substation['nominal_voltage'].values[0], # 15 kV
        'power': 10.e6  # 10 MW
    }


    active_load = test_feeder['buses'][['consumption_active_power']].copy()
    active_load.columns = [0]
    active_load.fillna(0., inplace=True)
    reactive_load = test_feeder['buses'][['consumption_reactive_power']].copy()
    reactive_load.columns = [0]
    reactive_load.fillna(0., inplace=True)

    results = feeder_powerflow(test_feeder['lines'],
                               test_feeder['buses'],
                               active_load,
                               reactive_load,
                               substation['nominal_voltage'].values[0],
                               substation['setpoint'].values[0],
                               per_unit_scale,
                               precision=1e-20,
                               max_iteration=10)

    comparison_voltage = pd.concat([test_feeder['buses'][['bus_id', 'id', 'voltage_norm']], results['voltage']], axis=1)
    comparison_voltage['v_target_scaled'] = comparison_voltage['voltage_norm'] / per_unit_scale['voltage']
    comparison_voltage['v_result_scaled'] = comparison_voltage[0] / per_unit_scale['voltage']
    comparison_voltage['scaled_diff'] = np.abs(comparison_voltage['v_target_scaled'] - comparison_voltage['v_result_scaled'])

    comparison_power = pd.concat([test_feeder['lines'][['branch_id','active_power', 'reactive_power']], results['active_flow'], results['reactive_flow']], axis=1)
    comparison_power.columns = ['branch_id', 'active_power_target', 'reactive_power_target', 'active_power', 'reactive_power']
    comparison_power['active_diff'] = np.abs(comparison_power['active_power_target'] - comparison_power['active_power'])
    comparison_power['reactive_diff'] = np.abs(comparison_power['reactive_power_target'] - comparison_power['reactive_power'])

    if to_plot:
        fig = plot_feeder_powerflow(test_feeder['buses'], test_feeder['lines'], results,
                                    nominal_voltage=substation['nominal_voltage'].values[0])
        fig.show()

    return test_feeder, results, comparison_voltage, comparison_power


def test_roseaux_cases():

    feeder_list = ['Feeder0000', 'Feeder0001', 'Feeder0002']

    for feeder in feeder_list:
        test_feeder, results, comparison_voltage, comparison_power = run_roseaux_test_case(feeder, False)
        comparison_voltage.to_csv(f'test_results/comparison_voltage_{feeder}.csv')
        comparison_power.to_csv(f'test_results/comparison_power_{feeder}.csv')

if __name__ == '__main__':
    test_powerflow_csv()
