import pandas as pd
import numpy as np
from dnpf.cython_extension import powerflow


def scale_powers(active_load, reactive_load, per_unit_scale):
    """
    Calculates the active and reactive load scaled using the per-unit scale

    :param active_load:
    :param reactive_load:
    :param per_unit_scale:
    :return:
    """

    active_load_scaled = active_load.values / per_unit_scale['power']
    reactive_load_scaled = reactive_load.values / per_unit_scale['power']

    return active_load_scaled, reactive_load_scaled


def scale_impedances(lines, per_unit_scale):
    """
    Calculates the active and reactive load scaled using the per-unit scale

    :param active_load:
    :param reactive_load:
    :param per_unit_scale:
    :return:
    """
    impedance_base = per_unit_scale['voltage'] ** 2 / per_unit_scale['power']

    lines['R_scaled'] = lines['R'] / impedance_base
    lines['X_scaled'] = lines['X'] / impedance_base


def init_voltages(bus_count, step_count,  nominal_voltage, set_point, per_unit_scale):
    """
    Calculates the initial voltages using the nominal voltage, set point and per-unit scale


    :param step_count:
    :param bus_count:
    :param nominal_voltage:
    :param set_point:
    :param per_unit_scale:
    :return:
    """

    return (nominal_voltage * set_point / per_unit_scale['voltage']) ** 2 * np.ones((bus_count, step_count))


def results_to_dataframe(power_flow_results, per_unit_scale, line_index, bus_index, load_index):
    voltage_df = pd.DataFrame(data=np.sqrt(power_flow_results[2]) * per_unit_scale['voltage'],
                              index=bus_index, columns=load_index, dtype=float)

    active_flow_df = pd.DataFrame(data=np.array(power_flow_results[0][1:, :]) * per_unit_scale['power'],
                                  index=line_index, columns=load_index, dtype=float)

    reactive_flow_df = pd.DataFrame(data=np.array(power_flow_results[1][1:, :]) * per_unit_scale['power'],
                                    index=line_index, columns=load_index, dtype=float)

    return {
        'voltage': voltage_df,
        'active_flow': active_flow_df,
        'reactive_flow': reactive_flow_df
    }


def feeder_powerflow(lines, buses, active_load, reactive_load, nominal_voltage, set_point, per_unit_scale,
                     precision=1e-8, max_iteration=10):
    """

    :param lines:
    :param buses:
    :param per_unit_scale:
    :param set_point:
    :param precision:
    :param max_iteration:
    :return:
    """
    bus_count, step_count = active_load.shape

    # Initialization of the cpp network data model

    scale_impedances(lines, per_unit_scale)

    cpp_model = powerflow.PyNetwork(lines['R_scaled'].values,
                                    lines['X_scaled'].values,
                                    lines['from'].values,
                                    lines['to'].values)

    active_load_scaled, reactive_load_scaled = scale_powers(active_load, reactive_load, per_unit_scale)

    initial_voltages = init_voltages(bus_count, step_count, nominal_voltage, set_point, per_unit_scale)

    # Backward Forward Sweep powerflow execution
    powerflow_results = cpp_model.multiBackwardForward_Sweep(initial_voltages,
                                                             active_load_scaled,
                                                             reactive_load_scaled,
                                                             precision,
                                                             max_iteration)

    powerflow_result_df = results_to_dataframe(powerflow_results,
                                               per_unit_scale,
                                               lines.index,
                                               buses.index,
                                               active_load.columns)

    result_df = {'active_load': active_load, 'reactive_load': reactive_load, **powerflow_result_df}

    return result_df


def network_powerflow(network_data, active_load, reactive_load, per_unit_scale, precision=1e-8, max_iteration=10):
    """

    :param network_data:
    :param active_load:
    :param reactive_load:
    :param per_unit_scale:
    :param precision:
    :param max_iteration:
    :return:
    """
    substation = network_data['substation']

    results = {}

    for feeder_id, feeder_data in network_data['feeders'].items():
        results[feeder_id] = feeder_powerflow(feeder_data['lines'],
                                              feeder_data['buses'],
                                              active_load[feeder_id],
                                              reactive_load[feeder_id],
                                              substation['nominal_voltage'].values[0],
                                              substation['setpoint'].values[0],
                                              per_unit_scale,
                                              precision=precision,
                                              max_iteration=max_iteration)

    return results
